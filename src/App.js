// IMPORT THE HOOK STATE b
import { useState, useEffect } from 'react'
import Form from './Form';
import Table from './Table';


function App() {
  // Define the URL
  const API_URL = 'https://jsonplaceholder.typicode.com/';



  // Define certain since after  the source hppen we going to change useState method
  const [reqType, setReqType] = useState('users');
  const [items, setItems] = useState([]);


  useEffect(() => {

    // Define the function fetchItems 
    const fetchItmes = async () => {
      try {
        const response = await fetch(`${API_URL}${reqType}`);
        const data = await response.json();
        console.log(data);
        setItems(data);
      } catch (err) {
        console.error(err)
      }
    }
    fetchItmes();

  }, [reqType])

  return (
    <div className="App">
      <Form reqType={reqType} setReqType={setReqType}
      />

      {/* <List items={items} /> */}
      <Table items={items} />
    </div>
  );
}

export default App;
